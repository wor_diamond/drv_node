const DynamicBuffer = require("@fidian/dynamic-buffer");
const fs = require("fs");
const mkdirp = require("mkdirp");
const util = require("../util");
const {unpackObject} = require("./parse");

const signature = new Buffer("WDRV".split("").map(chr => chr.charCodeAt(0)));

function handleObject(data, path) {
	if(data.type == "dir") {
		mkdirp.sync(path);

		data.data.forEach(child => {
			handleObject(child.data, path + "/" + child.name);
		});
	} else if(data.type == "file") {
		let fd = fs.openSync(path, "w");
		fs.writeSync(fd, data.data);
		fs.closeSync(fd);
	}
}

function decode(buf, path) {
	if(!util.slice(buf, 0, 4).equals(signature)) {
		throw new Error("Signature mismatch");
	}

	let loaderOffset = util.fromUint32(util.slice(buf, 4, 4)); // Loader
	let rootOffset = util.fromUint32(util.slice(buf, 8, 4)); // Root file

	let data = unpackObject(buf, rootOffset);
	handleObject(data, path);
}

module.exports = decode;