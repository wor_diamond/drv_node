const DynamicBuffer = require("@fidian/dynamic-buffer");

module.exports = {
	to(num, cnt) {
		let res = [];
		for(let i = 0; i < cnt; i++) {
			res.push(num % 0x100);
			num = Math.floor(num / (1 << 8));
		}
		return Buffer.from(res);
	},
	from(buf, cnt) {
		let res = 0;
		for(let i = buf.length - 1; i >= 0; i--) {
			res *= 1 << 8;
			res += buf[i];
		}
		return res;
	},

	toUint32(num) {
		return this.to(num, 4);
	},

	fromUint32(buf) {
		return this.from(buf, 4);
	},

	padZero(str, cnt) {
		let buf = Buffer.alloc(cnt);
		buf.write(str);
		return buf;
	},

	slice(buf, offset, length) {
		if(buf instanceof DynamicBuffer) {
			buf = buf.getBuffer();
		}

		return buf.slice(offset, offset + length);
	},

	formatAddress(num) {
		num = num.toString(16).toUpperCase();
		return "0x" + "0".repeat(8 - num.length) + num;
	}
};