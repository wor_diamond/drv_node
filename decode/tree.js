const util = require("../util");
const {unpackObject} = require("./parse");

const signature = new Buffer("WDRV".split("").map(chr => chr.charCodeAt(0)));

function printObject(data, padding, name, address) {
	// Address
	process.stdout.write("[" + util.formatAddress(address) + "] ");

	// Permissions
	let permissions = "";
	permissions += data.type == "dir" ? "d" : "-";

	// Owner
	permissions += data.permissions.owner & 0x1 ? "r" : "-";
	permissions += data.permissions.owner & 0x2 ? "w" : "-";
	permissions += data.permissions.owner & 0x4 ? "x" : "-";

	// Group
	permissions += data.permissions.group & 0x1 ? "r" : "-";
	permissions += data.permissions.group & 0x2 ? "w" : "-";
	permissions += data.permissions.group & 0x4 ? "x" : "-";

	// Everyone
	permissions += data.permissions.everyone & 0x1 ? "r" : "-";
	permissions += data.permissions.everyone & 0x2 ? "w" : "-";
	permissions += data.permissions.everyone & 0x4 ? "x" : "-";

	// Owner name
	let owner = data.owner;

	process.stdout.write("[" + permissions + " " + owner + ".".repeat(32 - owner.length) + "] ");

	if(data.type == "dir") {
		process.stdout.write(padding + name + "/\n");

		data.data.forEach(child => {
			printObject(child.data, padding + "  ", child.name, child.address);
		});
	} else if(data.type == "file") {
		process.stdout.write(padding + name + "\n");
	}
}

function printTree(buf) {
	if(!util.slice(buf, 0, 4).equals(signature)) {
		throw new Error("Signature mismatch");
	}

	let loaderOffset = util.fromUint32(util.slice(buf, 4, 4)); // Loader
	let rootOffset = util.fromUint32(util.slice(buf, 8, 4)); // Root file

	let data = unpackObject(buf, rootOffset);

	if(loaderOffset != 0) {
		process.stdout.write("[" + util.formatAddress(loaderOffset) + "] ");
		process.stdout.write("[:--------- root............................] ");
		process.stdout.write("<LOADER>");
		process.stdout.write("\n");
	}

	printObject(data, "", "", rootOffset);
}

module.exports = printTree;