const argv = require("command-line-args")([
	{name: "help", alias: "h", type: Boolean},
	{name: "encode", alias: "e", type: String},
	{name: "rules", alias: "R", type: String},
	{name: "decode", alias: "d", type: String},
	{name: "tree", alias: "t", type: Boolean},
	{name: "cat", alias: "c", type: String},
]);

if(argv.help) {
	console.log("DRV format implementation for NodeJS");
	console.log("");
	console.log("Usage:");
	console.log("--help, -h                      Show this help");
	console.log("--encode <dir>, -e <dir>        Encode a directory and output the image");
	console.log("--rules <file>, -R <file>       Encode an image using rules file");
	console.log("--decode <dist>, -d <dist>      Decode a DRV file from stdin and output to <dist>");
	console.log("--tree, -t                      Decode a DRV file from stdin and print its tree");
	console.log("--cat <file>, -c <file>         Decode a DRV file from stdin and output file <file>");
	console.log("--cat '<loader>', -c '<loader>' Decode a DRV file from stdin and output loader");
	console.log("");
	console.log("Rules file is newline-separated array of rules. Each rule has an action and an");
	console.log("argument, like this: 'ignore:rules.txt'");
	console.log("The actions are:");
	console.log("1. ignore");
	console.log("Don't add files specified by glob (argument) to drive image.");
	console.log("2. loader");
	console.log("Add file specified by argument as loader.");
	console.log("The file is usually named 'rules.txt', and it is a common practice to add");
	console.log("'ignore:rules.txt' line to rules file.");
} else if(argv.encode) {
	let rules = "";
	if(argv.rules) {
		rules = require("fs").readFileSync(argv.rules, {
			encoding: "utf-8"
		});
	}

	let path = argv.encode;
	let buffer = require("./encode")(path, rules);
	process.stdout.write(buffer);
} else if(argv.decode) {
	let path = argv.decode;

	const DynamicBuffer = require("@fidian/dynamic-buffer");

	let buf = new DynamicBuffer();
	process.stdin.on("data", data => {
		buf.concat(data);
	}).on("end", () => {
		require("./decode")(buf, path);
	});
} else if(argv.tree) {
	const DynamicBuffer = require("@fidian/dynamic-buffer");

	let buf = new DynamicBuffer();
	process.stdin.on("data", data => {
		buf.concat(data);
	}).on("end", () => {
		require("./decode/tree")(buf);
	});
} else if(argv.cat) {
	let path = argv.cat;

	const DynamicBuffer = require("@fidian/dynamic-buffer");

	let buf = new DynamicBuffer();
	process.stdin.on("data", data => {
		buf.concat(data);
	}).on("end", () => {
		require("./decode/cat")(buf, path);
	});
}