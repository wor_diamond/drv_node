const DynamicBuffer = require("@fidian/dynamic-buffer");
const fs = require("fs");
const util = require("../util");
const minimatch = require("minimatch");

const signature = new Buffer("WDRV".split("").map(chr => chr.charCodeAt(0)));

function packFile(path) {
	let buf = fs.readFileSync(path);
	let originalSize = buf.length;

	// Fragments
	let fragments = [];
	for(let i = 0; i < originalSize - 4096; i += 4096) {
		fragments.push({buf: buf.slice(i, i + 4096), size: 4096});
	}

	// Pad to 4096 multiple
	let pos = Math.floor(originalSize / 4096) * 4096;
	let padded = buf.slice(pos);
	padded = Buffer.concat([padded, Buffer.alloc(originalSize - pos)]);
	fragments.push({buf: padded, size: originalSize - pos});

	return {
		fragments: fragments,
		additional: Buffer.alloc(0)
	};
}
function packDirectory(path, offset, rules) {
	let files = fs.readdirSync(path, {
		encoding: "buffer"
	});
	files = files.filter(file => {
		return !rules.ignore.some(rule => minimatch((path + "/" + file.toString("utf-8")).substr(rules.originalPath.length), rule));
	});

	// Think about resulting object size
	let sum = 0;
	sum += 4; // File count
	files.forEach(name => {
		sum += name.length; // File name
		sum++; // Null separator
		sum += 4; // Address
	});

	let originalSize = sum;

	// Pad to 4096 multiple
	sum = Math.ceil(sum / 4096) * 4096;

	// Gather all children data
	let children = new DynamicBuffer();
	let childrenAddress = [];
	files.forEach(name => {
		// Full child path
		let full = path + "/" + name.toString("utf8");

		// Pack children recursively
		let child = packObject(full, offset + sum, rules);
		childrenAddress.push(offset + sum);
		sum += child.length;
		children.concat(child);
	});


	let buf = new DynamicBuffer();

	buf.concat(util.toUint32(files.length)); // File count

	files.forEach((name, i) => {
		buf.concat(name); // File name
		buf.write(0x00); // Null separator
		buf.concat(util.toUint32(childrenAddress[i])); // Address

		originalSize += name.length + 5;
	});

	// Pad to 4096 multiple
	let left = Math.ceil(buf.length / 4096) * 4096 - buf.length;
	buf.concat(Buffer.alloc(left));

	return {
		fragments: [{buf, size: originalSize}],
		additional: children
	};
}

function packObject(path, offset, rules) {
	// Construct object data
	let stats = fs.statSync(path);

	let object;
	if(stats.isFile()) {
		object = packFile(path);
	} else if(stats.isDirectory()) {
		object = packDirectory(path, offset + 44, rules);
	}

	let buf = new DynamicBuffer();

	object.fragments.forEach((fragment, i) => {
		if(stats.isFile()) {
			buf.write(0x00); // File type
		} else if(stats.isDirectory()) {
			buf.write(0x01); // File type
		}
		offset++;

		// Permissions
		buf.write(0b111); // Owner
		buf.write(0b101); // Group
		buf.write(0b101); // Everyone
		offset += 3;

		buf.concat(util.toUint32(fragment.size)); // File size
		offset += 4;

		// Calculate link to next fragment
		let nextFragment = offset + 36 + fragment.buf.length;
		if(i == object.fragments.length - 1) {
			nextFragment = 0;
		}

		buf.concat(util.toUint32(nextFragment)); // Next fragment
		offset += 4;

		buf.concat(util.padZero("root", 32)); // Owner
		offset += 32;

		buf.concat(fragment.buf); // Object itself
		offset += fragment.buf.length;
	});

	// Additional data
	buf.concat(object.additional);

	return buf;
}

function encode(path, ruleString) {
	let rules = {
		ignore: [],
		loader: null,
		originalPath: path + "/"
	};
	ruleString.split("\n").map(row => {
		// Empty string or comment
		if(row == "" || row.trim()[0] == ";") {
			return;
		}

		let action = row.substr(0, row.indexOf(":")).trim().toLowerCase();
		let rule = row.substr(row.indexOf(":") + 1).trim();

		if(action == "ignore") {
			rules.ignore.push(rule);
		} else if(action == "loader") {
			rules.loader = fs.readFileSync(require("path").join(path, rule));
		} else {
			throw new Error("Unknown action " + action + " in rules file")
		}
	});

	let buf = new DynamicBuffer();

	buf.concat(signature); // Signature

	// Loader
	if(rules.loader) {
		buf.concat(util.toUint32(buf.length + 8)); // Address
		buf.concat(util.toUint32(buf.length + 4 + rules.loader.length + 4)); // Root file
		buf.concat(rules.loader);
		buf.concat(util.toUint32(0xDEADBEEF));
	} else {
		buf.concat(util.toUint32(0)); // Address
		buf.concat(util.toUint32(buf.length + 4)); // Root file
	}

	buf.concat(packObject(path, buf.length, rules));
	return buf.getBuffer();
}

module.exports = encode;