const DynamicBuffer = require("@fidian/dynamic-buffer");
const util = require("../util");

let signature = new Buffer("WDRV".split("").map(chr => chr.charCodeAt(0)));

function unpackObject(buf, offset) {
	let originalOffset = util.formatAddress(offset);

	let data = new DynamicBuffer();

	let expectedType = null;
	let expectedPermissions = null;
	let expectedOwner = null;

	while(offset != 0) {
		let fragmentOffset = util.formatAddress(offset);

		// File type
		let type = buf.getBuffer()[offset];
		offset++;
		if(expectedType === null) {
			expectedType = type;
		} else if(expectedType != type) {
			throw new Error("Inconsistency of type between fragments: expected " + expectedType + " (at " + originalOffset + "), got " + type + " (at " + fragmentOffset + ")");
		}

		// Permissions
		let permissions = {
			owner:    buf.getBuffer()[offset + 0],
			group:    buf.getBuffer()[offset + 1],
			everyone: buf.getBuffer()[offset + 2]
		};
		offset += 3;
		if(expectedPermissions === null) {
			expectedPermissions = permissions;
		} else if(permissions.owner != expectedPermissions.owner) {
			throw new Error("Inconsistency of owner permissions between fragments: expected " + expectedPermissions.owner + " (at " + originalOffset + "), got " + permissions.owner + " (at " + fragmentOffset + ")");
		} else if(permissions.group != expectedPermissions.group) {
			throw new Error("Inconsistency of group permissions between fragments: expected " + expectedPermissions.group + " (at " + originalOffset + "), got " + permissions.group + " (at " + fragmentOffset + ")");
		} else if(permissions.everyone != expectedPermissions.everyone) {
			throw new Error("Inconsistency of everyone permissions between fragments: expected " + expectedPermissions.everyone + " (at " + originalOffset + "), got " + permissions.everyone + " (at " + fragmentOffset + ")");
		}

		let fragmentSize = util.fromUint32(util.slice(buf, offset, 4)); // Fragment size
		offset += 4;

		let nextFragment = util.fromUint32(util.slice(buf, offset, 4)); // Next fragment
		offset += 4;

		// Owner
		let ownerBuffer = util.slice(buf, offset, 32);
		let owner = new DynamicBuffer();
		for(let i = 0; i < ownerBuffer.length && ownerBuffer[i] != 0; i++) {
			owner.write(ownerBuffer[i]);
		}
		owner = owner.getBuffer().toString("utf-8");
		offset += 32;
		if(expectedOwner === null) {
			expectedOwner = owner;
		} else if(expectedOwner != owner) {
			throw new Error("Inconsistency of owner between fragments: expected " + expectedOwner + " (at " + originalOffset + "), got " + owner + " (at " + fragmentOffset + ")");
		}

		// Get data part
		let dataPart = util.slice(buf, offset, fragmentSize);
		data.concat(dataPart);

		offset = nextFragment;
	}

	if(expectedType == 0) {
		return {type: "file", data: unpackFile(data, buf), permissions: expectedPermissions, owner: expectedOwner};
	} else if(expectedType == 1) {
		return {type: "dir", data: unpackDirectory(data, buf), permissions: expectedPermissions, owner: expectedOwner};
	}
}

function unpackFile(data) {
	return data.getBuffer();
}

function unpackDirectory(data, buf) {
	let fileCount = util.fromUint32(util.slice(data, 0, 4)); // File count
	let localOffset = 4;

	let files = [];
	for(let i = 0; i < fileCount; i++) {
		// File name
		let name = new DynamicBuffer();
		while(data.getBuffer()[localOffset] != 0) {
			name.write(data.getBuffer()[localOffset]);
			localOffset++;
		}
		name = name.getBuffer().toString("utf-8");

		// Null terminator
		localOffset++;

		// Address
		let address = util.fromUint32(util.slice(data, localOffset, 4));
		localOffset += 4;

		files.push({name, address});
	}

	files.forEach(file => {
		file.data = unpackObject(buf, file.address);
	});

	return files;
}

module.exports = {
	unpackObject,
	unpackDirectory,
	unpackFile
};