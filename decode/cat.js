const util = require("../util");
const {unpackObject} = require("./parse");

const signature = new Buffer("WDRV".split("").map(chr => chr.charCodeAt(0)));

function cat(buf, path) {
	if(!util.slice(buf, 0, 4).equals(signature)) {
		throw new Error("Signature mismatch");
	}

	let loaderOffset = util.fromUint32(util.slice(buf, 4, 4)); // Loader
	let rootOffset = util.fromUint32(util.slice(buf, 8, 4)); // Root file

	if(path.toLowerCase() == "<loader>") {
		if(loaderOffset == 0) {
			throw new Error("No loader present")
		}

		let end = loaderOffset;
		while(util.fromUint32(util.slice(buf, end, 4)) != 0xDEADBEEF) {
			end++;
		}

		process.stdout.write(util.slice(buf, loaderOffset, end - loaderOffset));
	} else {
		let data = unpackObject(buf, rootOffset);

		path.split("/").forEach((part, i) => {
			if(data.type != "dir") {
				throw new Error("Not a directory: /" + path.split("/").slice(0, i).join("/"));
			}

			data = data.data.find(file => file.name == part);
			if(!data) {
				throw new Error("No such file: /" + path.split("/").slice(0, i + 1).join("/"));
			}
			data = data.data;
		});

		if(data.type != "file") {
			throw new Error("Not a file: /" + path);
		}

		process.stdout.write(data.data);
	}
}

module.exports = cat;